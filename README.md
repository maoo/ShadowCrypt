# ShadowCrypt

See https://shadowcrypt-release.weebly.com/ (source code on  https://github.com/sunblaze-ucb/shadowcrypt)

Plan is to integrate it with [keybase.io](keybase.io), using [kbpgp](https://keybase.io/kbpgp).

Extension will allow to choose if content is visibly by:
1. Anyone who installed the app (relies on other permissions)
2. Any keybase.io friend
3. Any member of a given keybase team

## Test ShadowCrypt

1. Open [Google Chrome Extensions](chrome://extensions/)
2. Enable `Developer Mode` toggle (top right)
3. Click on `Load unpacked` button (top left)
4. Point to the `ext` folder of the project
5. Visit https://twitter.com
6. Click on the black locker icon on the right side of the input field
7. Type your message and hit `Tweet`
